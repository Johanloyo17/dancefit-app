import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
// import { reject } from 'core-js/fn/promise'
Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		// currentUser:false,
		userActive:null
	},
	getters:{
		setUserData(state){
			return {
				data: state.userActive
			}
		},
		currentUser(state){
			let isUser =  state.userActive ? true : false
			return isUser
		}
		
	},
	mutations: {
		ADD_USERINFO: function(state, user) {
			// console.log(user)
			state.userActive = user
			user ? localStorage.setItem('currentUser', true): localStorage.setItem('currentUser', false)
		},
		RESET_USER: (state) => {
			console.log('reseteando')
			state.userActive = null
			localStorage.removeItem('token')
			localStorage.removeItem('currentUser')
		}
	},
	actions: {
		// User information
		fetchUser(context) {
			console.log('fetch NOW')
			console.log('agregando datos de usuario')
			return	new Promise ((resolve, rejet) => {
				axios({
					method:'get',
					url: 'http://localhost:3002/user/',
					withCredentials: true
				})
				.then(user => {
					context.commit('ADD_USERINFO', user.data)
					resolve()
					console.log(user.data)

				})
				.catch(response  => {
					console.log(response.response)
					rejet(response.response)
				})
			})
		},

		authRequest(context, user){
			return new Promise ((resolve, reject) =>{
				// console.log(user)
				axios({
					method:'post',
					url:'http://localhost:3002/session/login/',
					data: {
						email: user.email,
						password: user.password,
					},
					withCredentials: true

				}).then(response => {
					context.dispatch('fetchUser')
					.then(() => {
						resolve(response)
					}).catch(err => console.log( err.response))
					
				}) .catch(err => {
						reject(err)
					})
			})
		},
		
		logoutRequest(context){
			return new Promise ((resolve, reject) =>{
				axios
				.delete("http://localhost:3002/session/logout/", {
					validateStatus: function(status) {
						return status >= 200 && status <= 500; // default
					}
				})
				.then((response)=> {
					console.log(response)
					context.commit('RESET_USER')
					resolve(response)
				}).catch(err =>{
					reject(err)
				})
			})
		}
	},
	
	modules: {
	}
})

import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/_auth/Sign_in.vue'
import Register from '../views/_auth/Sign_up.vue'
import Panel from '../views/Dashboard.vue'
import Galery from '../views/_galery/Galery.vue'
import AdminPanel from '../views/_panel/AdminPanel.vue'
import ContentPanel from '../views/_panel/ContentPanel.vue'

Vue.use(VueRouter)


  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/Login',
    name: 'Login',
    component: Login
  },
  {
      path: '/Register',
      name: 'Register',
      component: Register
  },
  //ruta privada PANEL
  {
      path: '/Panel',
      name: '',
      component: Panel,
      children:[
          {
            name: '',
            path:'',
            component: AdminPanel
        },
        {
            name: 'contentPanel',
            path:'contentPanel',
            component: ContentPanel
        }
    ],
      meta: {
              isPrivate: true
          }
    },
    
    // ruta privada galery
  {
      path: '/galery',
      name: 'galery',
      component: Galery,
    //   meta: {
    //       isPrivate: true
    //   }
  },
  {
    path: '/Test',
    name: 'Test',
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
    let isPrivate = to.matched.some((record) => record.meta.isPrivate)
    let isUser = localStorage.getItem('currentUser')
    
    isPrivate ? (
        isUser ? next() :
        next({
            name: 'Login'
        })
    ) : next()
})

export default router
